package moe.tjm;

import moe.tjm.model.IConnProvider;
import moe.tjm.model.IRCConnection;
import moe.tjm.model.ThreadRunningException;
import moe.tjm.model.database.DBBroker;
import moe.tjm.model.SSLConnProvider;
import org.ini4j.Ini;
import org.ini4j.Profile;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ConfigFileParser {
    static final String server_session_name = "Server";
    static final String database_session_name = "Database";

    static List<Thread> parse(String filepath) throws IOException {
        List<Thread> threads = new LinkedList<Thread>();
        Ini ini = new Ini(new File(filepath));
        Ini.Section database_session = ini.get(database_session_name);
        final String database_url = database_session.get("url");
        final String database_username = database_session.get("username");
        final String database_password = database_session.get("password");
        DBBroker database = null;
        try {
            database = new DBBroker(database_url, database_username, database_password);
            threads.add(database);
        } catch (SQLException e){
            // TODO: handle exception
            return threads;
        }
        Ini.Section server_session = ini.get(server_session_name);
        for(String section_name: server_session.keySet()){
            Profile.Section section = ini.get(section_name);
            final String servername = section_name;
            final String host = section.get("address");
            final int port = Integer.valueOf(section.get("port"));
            IConnProvider provider = new SSLConnProvider(host,port);
            IRCConnection connection = new IRCConnection(servername,database);
            try {
                connection.setIConnProvider(provider);
                final String nick = section.get("nick");
                connection.setNick(nick);
                final String username = section.get("username");
                connection.setUsername(username);
                final String realname = section.get("realname");
                connection.setRealname(realname);
                final String channels = section.get("channels");
                connection.setChannels(channels);
                threads.add(connection);
            } catch (ThreadRunningException e){
                //TODO: handle exception
                continue;
            }
        }
        return threads;
    }
}
