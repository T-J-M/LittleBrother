package moe.tjm;

import moe.tjm.model.ThreadRunningException;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Main {

    private static String config_file_path;
    private static boolean daemon = false;

    public static void main(String[] args) {

        Options options = new Options();
        options.addOption("h",false,"display this message and exit");
        options.addOption("c",true,"path of config file");
        options.addOption("d",false,"run in daemon mode");

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);
            if(cmd.hasOption("h")){
                help();
                return;
            }
            if(cmd.hasOption("c")){
                config_file_path = cmd.getOptionValue("c");
            } else {
                help();
                return;
            }
            if(cmd.hasOption("d"))
                daemon = true;
        } catch (ParseException e){
            help();
            return;
        }

        List<Thread> threads = new LinkedList<Thread>();
        try {
            threads = ConfigFileParser.parse(config_file_path);
        } catch (IOException e){
            // TODO: handle exception
        }

        if(daemon){
            //TODO: implement daemon and fork
        }
        for(Thread t: threads)
            t.run();
        for(Thread t: threads)
            try {
                t.join();
            } catch (InterruptedException e){
                //TODO: handle exception
            } finally {

            }
    }

    public static void help(){
        // TODO: implement help message
    }
}
