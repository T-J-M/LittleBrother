package moe.tjm.model.database;

import moe.tjm.model.message.IMessage;
import moe.tjm.model.message.Message;
import moe.tjm.model.message.MessageSender;

public class LogEntry {
    private String servername;
    private IMessage message;

    public LogEntry(String servername, IMessage msg){
        this.servername = servername;
        this.message = msg;
    }

    public String getServername(){
        return servername;
    }

    public String getSenderNick(){
        return message.getSender().getFrom();
    }

    public String getSenderUsername(){
        return message.getSender().getUsername();
    }

    public String getSenderHostname(){
        return message.getSender().getHostname();
    }

    public String getChannel(){
        return message.getTarget();
    }

    public String getContent(){
        return message.getContent();
    }
}
