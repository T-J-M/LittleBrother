package moe.tjm.model.database;

import java.sql.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

public class DBBroker extends Thread implements IDBBroker{

    protected Connection dbconn;
    protected ReentrantLock queue_lock;
    protected Queue<LogEntry> entry_queue;
    protected Semaphore queue_sem;


    public DBBroker(String url, String username, String password) throws SQLException {
        this.dbconn = DriverManager.getConnection(url,username,password);
        this.queue_lock = new ReentrantLock();
        this.entry_queue = new LinkedList<LogEntry>();
        this.queue_sem = new Semaphore(0);
    }

    public boolean verhoog(LogEntry entry){
        boolean rst = false;
        queue_lock.lock();
        try {
            entry_queue.add(entry);
            queue_sem.release();
            rst = true;
        } finally {
            queue_lock.unlock();
        }
        return rst;
    }

    private void insertEntry(LogEntry entry){
        String SQL = "INSERT into message(servername,nick,username,hostname,channel,content) "
                +"VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement pstmt = dbconn.prepareStatement(SQL);
            pstmt.setString(1, entry.getServername());
            pstmt.setString(2, entry.getSenderNick());
            pstmt.setString(3, entry.getSenderUsername());
            pstmt.setString(4, entry.getSenderHostname());
            pstmt.setString(5, entry.getChannel());
            pstmt.setString(6, entry.getContent());
            pstmt.executeUpdate();
        } catch (SQLException e){
            // TODO: handle exception here
        }
    }

    @Override
    public void run() {
        for(;;) {
            try {
                queue_sem.acquire();
                queue_lock.lock();
                LogEntry entry = entry_queue.poll();
                insertEntry(entry);
                queue_lock.unlock();
            } catch (InterruptedException e) {
                // TODO: handle exception here
            } finally {
                Thread.yield();
            }
        }
    }
}
