package moe.tjm.model;

public class IConnException  extends Exception {

    public IConnException(String msg){
        super(msg);
    }
}
