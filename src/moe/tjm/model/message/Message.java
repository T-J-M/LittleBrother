package moe.tjm.model.message;

import moe.tjm.model.database.IDBBroker;

public abstract class Message implements IMessage{
    private final String content;
    private final MessageSender sender;
    private final String target;

    protected Message(MessageSender sender, String content, String target){
        this.content = content;
        this.sender = sender;
        this.target = target;
    }

    protected Message(IMessage msg){
        this.content = msg.getContent();
        this.sender = msg.getSender();
        this.target = msg.getTarget();
    }

    public String getContent(){
        return  this.content;
    }

    public MessageSender getSender(){
        return this.sender;
    }
    public String getTarget(){
        return this.target;
    }

    public abstract String[] handle(IDBBroker dbconn);
}
