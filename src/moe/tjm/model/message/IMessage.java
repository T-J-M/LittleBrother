package moe.tjm.model.message;

import moe.tjm.model.database.IDBBroker;

public interface IMessage {
    public String getContent();
    public MessageSender getSender();
    public String getTarget();
    public String[] handle(IDBBroker dbconn);
}
