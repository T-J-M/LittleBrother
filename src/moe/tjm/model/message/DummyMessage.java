package moe.tjm.model.message;

import moe.tjm.model.database.IDBBroker;

public class DummyMessage extends Message implements IMessage {

    public DummyMessage(MessageSender sender, String content, String target){
         super(sender,content,target);
    }

    public DummyMessage(IMessage msg){
        super(msg);
    }

    public String[] handle(IDBBroker dbconn){
        return new String[]{};
    }
}
