package moe.tjm.model.message.factory;

import moe.tjm.model.message.DummyMessage;
import moe.tjm.model.message.IMessage;
import moe.tjm.model.message.MessageSender;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class MessageFacotry {
    static protected String pattern = "";
    static protected MessageFacotry instance = null;

    protected MessageFacotry(){}

    protected static void register(){
        Pattern pattern1 = Pattern.compile(pattern);
        MessageFactoryDispatcher.register(pattern1,getInstance());
    }

    public IMessage make(String rawMsg){
        MessageSender sender = null;
        String content = "";
        String target = "";
        String message_regex = "^(?<prefix>:\\S+)?\\s(?<content>.+)$";
        Pattern message_pattern = Pattern.compile(message_regex);
        Matcher message_matcher = message_pattern.matcher(rawMsg);
        if(message_matcher.matches()){
            String prefix = message_matcher.group("prefix");
            String message_content = message_matcher.group("content");
            //Parse message prefix
            sender = MessageSenderFactory.getInstance().make(prefix);
            //Parse message content
            String conent_regex = "^(?<command>\\w+)\\s(?<target>[#:]\\S+)(\\s(?<content>.+))?$";
            Pattern content_pattern = Pattern.compile(conent_regex);
            Matcher content_matcher = content_pattern.matcher(message_content);
            if(content_matcher.matches()){
                target = content_matcher.group("target");
                content = content_matcher.group("content");
            }
        }
        return new DummyMessage(sender, content, target);
    }

    public static MessageFacotry getInstance(){
        if(null==instance)
            instance = DummyMessageFactory.getInstance();
        return instance;
    }
}
