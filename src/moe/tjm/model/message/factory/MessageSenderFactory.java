package moe.tjm.model.message.factory;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import moe.tjm.model.message.MessageSender;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageSenderFactory {
    private static MessageSenderFactory instance = null;

    private MessageSenderFactory(){}

    public static MessageSenderFactory getInstance(){
        if(instance==null){
            instance = new MessageSenderFactory();
        }
        return instance;
    }

    public MessageSender make(String raw_data){
        MessageSender sender = null;
        String prefix_regex = "^:(?<nick>[A-Za-z0-9_-]+)!(?<username>\\S+)@(?<hostname>\\S+)\\s*$";
        Pattern prefix_pattern = Pattern.compile(prefix_regex);
        Matcher prefix_matcher = prefix_pattern.matcher(raw_data);
        if(prefix_matcher.matches()){
            String nick = prefix_matcher.group("nick");
            String username = prefix_matcher.group("username");
            String hostname = prefix_matcher.group("hostname");
            sender = new MessageSender(nick,username,hostname);
        } else {
            sender = new MessageSender(raw_data);
        }
        return sender;
    }
}
