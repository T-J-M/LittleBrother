package moe.tjm.model.message.factory;

import moe.tjm.model.message.IMessage;
import moe.tjm.model.message.PingMessage;

public class PingMessageFactory extends MessageFacotry {

    static {
        //TODO: try to use structured regex other than hardcoded ones
        pattern = "^(.+\\s)?PING\\s(.*)";
        register();
    }

    private PingMessageFactory(){}

    public static MessageFacotry getInstance(){
        if(null==instance)
            instance = new PingMessageFactory();
        return instance;
    }

    public IMessage make(String rawMsg){
        return new PingMessage(super.make(rawMsg));
    }
}
