package moe.tjm.model.message.factory;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageFactoryDispatcher {
    private static HashMap<Pattern, MessageFacotry> register_map = new HashMap<>();

    public static MessageFacotry getFactory(String rawMsg){
        for(Map.Entry<Pattern, MessageFacotry> entry: register_map.entrySet()){
            Pattern pattern = entry.getKey();
            MessageFacotry facotry = entry.getValue();
            Matcher matcher = pattern.matcher(rawMsg);
            if(matcher.matches()){
                return facotry;
            }
        }
        return DummyMessageFactory.getInstance();
    }

    public static void register(Pattern pattern, MessageFacotry facotry){
        register_map.put(pattern,facotry);
    }
}
