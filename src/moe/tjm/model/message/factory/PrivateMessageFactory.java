package moe.tjm.model.message.factory;

import moe.tjm.model.message.IMessage;
import moe.tjm.model.message.PingMessage;
import moe.tjm.model.message.PrivateMessage;

public class PrivateMessageFactory extends MessageFacotry {

    static {
        //TODO: try to use structured regex other than hardcoded ones
        pattern = "^(:(.+\\s))?PRIVMSG\\s(#\\w+)\\s:(.*)";
        /* Group paramaters:
            $0 stands for origional message
            $1 stands for raw sender with colon
            $2 stands for raw sender
            $3 stands for channel
            $4 stands for message content
         */
        register();
    }

    private PrivateMessageFactory(){}

    public static MessageFacotry getInstance(){
        if(null==instance)
            instance = new PrivateMessageFactory();
        return instance;
    }

    public IMessage make(String rawMsg){
        return new PrivateMessage(super.make(rawMsg));
    }
}
