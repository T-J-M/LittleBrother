package moe.tjm.model.message.factory;

import moe.tjm.model.message.IMessage;

public class DummyMessageFactory extends MessageFacotry {

    private DummyMessageFactory(){}

    public static MessageFacotry getInstance(){
        if(null==instance)
            instance = new DummyMessageFactory();
        return instance;
    }

    public IMessage make(String rawMsg){
        return super.make(rawMsg);
    }
}
