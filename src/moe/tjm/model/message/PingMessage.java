package moe.tjm.model.message;

import moe.tjm.model.database.IDBBroker;

public class PingMessage extends Message implements IMessage {

    public PingMessage(MessageSender sender, String content, String target){
         super(sender,content,target);
    }

    public PingMessage(IMessage msg){
        super(msg);
    }

    public String[] handle(IDBBroker dbconn){
        final String command = "PONG";
        String response = new String(command) + " ";
        response.concat(getSender().toString());
        String content = getContent();
        if(!content.isEmpty())
            response.concat(content);
        return new String[]{response};
    }
}
