package moe.tjm.model.message;

public class MessageSender {
    private String from;
    private String username;
    private String hostname;

    public MessageSender(String from){
         this(from,"","");
    }
    public MessageSender(String from, String username, String hostname){
        this.from = from;
        this.username = username;
        this.hostname = hostname;
    }

    public String getFrom(){
        return this.from;
    }

    public String getUsername(){
        return this.username;
    }

    public String getHostname(){
        return this.hostname;
    }

    @Override
    public String toString() {
        String rst = this.from;
        if(!this.username.isEmpty())
            rst.concat("!"+this.username);
        if(!this.hostname.isEmpty())
            rst.concat("@"+this.hostname);
        return rst;
    }
}
