package moe.tjm.model.message;

import moe.tjm.model.database.IDBBroker;
import moe.tjm.model.database.LogEntry;

public class PrivateMessage extends Message implements IMessage {

    public PrivateMessage(MessageSender sender, String content, String target){
         super(sender,content,target);
    }

    public PrivateMessage(IMessage msg){
        super(msg);
    }

    public String[] handle(IDBBroker dbconn){
        String name = Thread.currentThread().getName();
        LogEntry entry = new LogEntry(name,this);
        dbconn.verhoog(entry);
        //no response needed
        return new String[]{};
    }
}
