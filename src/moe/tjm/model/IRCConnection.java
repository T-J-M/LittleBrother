package moe.tjm.model;

import moe.tjm.model.database.IDBBroker;
import moe.tjm.model.message.IMessage;
import moe.tjm.model.message.factory.MessageFacotry;
import moe.tjm.model.message.factory.MessageFactoryDispatcher;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

public class IRCConnection extends Thread {

    private IDBBroker dbconn; //Observer
    private IConnProvider inet_conn;
    private boolean runLock;

    private String nick;
    private String username;
    private String realname;
    private String[] channels;

    public IRCConnection(String name, IDBBroker dbconn) {
        super(name);
        this.dbconn = dbconn;
        this.runLock = false;
    }

    public void setNick(String nick) throws ThreadRunningException{
        if(runLock){
            throw new ThreadRunningException("Trying to set connection provider while thread in executing");
        } else {
            this.nick = nick;
        }
    }

    public void setUsername(String username) throws ThreadRunningException{
        if(runLock){
            throw new ThreadRunningException("Trying to set connection provider while thread in executing");
        } else {
            this.username = username;
        }
    }

    public void setRealname(String realname) throws ThreadRunningException{
        if(runLock){
            throw new ThreadRunningException("Trying to set connection provider while thread in executing");
        } else {
            this.realname = realname;
        }
    }

    public void setChannels(String channels) throws ThreadRunningException{
        if(runLock){
            throw new ThreadRunningException("Trying to set connection provider while thread in executing");
        } else {
            this.channels = channels.split(",");
        }
    }

    public void setIConnProvider(IConnProvider provider) throws ThreadRunningException {
        if(runLock){
            throw new ThreadRunningException("Trying to set connection provider while thread in executing");
        } else {
            this.inet_conn = provider;
        }
    }

    private static void bufferWriteLine(BufferedWriter writer, String line) throws IOException{
        writer.write(line);
        writer.newLine();
        writer.flush();

    }

    private void sessionInit(BufferedReader reader, BufferedWriter writer) throws IOException{
        String feedback;
        bufferWriteLine(writer,"Nick "+nick+'\r'+'\n');
        //TODO: handle feed back info
        feedback = reader.readLine();
        //UNUSED
        feedback.isEmpty();

        bufferWriteLine(writer,"USER "+username+" "+username+" "+username+" :"+realname+'\r'+'\n');
        //TODO: handle feed back info
        feedback = reader.readLine();
        //UNUSED
        feedback.isEmpty();

        for(String channel: channels){
            bufferWriteLine(writer,"JOIN "+channel+'\r'+'\n');
            //TODO: handle feed back info
            feedback = reader.readLine();
            //UNUSED
            feedback.isEmpty();
        }
    }

    @Override
    public void run() {

        if(this.runLock)
            throw new RuntimeException("IRC connectin "+this.getName()+" is already in execution");

        this.runLock = true;
        try {
            inet_conn.connect();
            BufferedWriter connWriter = inet_conn.getWriter();
            BufferedReader connReader = inet_conn.getReader();
            sessionInit(connReader,connWriter);
            for(;;){
                if(connReader.ready()){
                    String raw_msg = connReader.readLine();
                    MessageFacotry facotry = MessageFactoryDispatcher.getFactory(raw_msg);
                    IMessage msg = facotry.make(raw_msg);
                    String[] feedbacks = msg.handle(dbconn);
                    for(String feedback: feedbacks)
                        bufferWriteLine(connWriter,feedback);
                } else {
                    yield();
                }
            }
        } catch (IConnException e){
            // TODO: handle exception
        } catch (IOException e) {
            // TODO: handle exception
        } finally {
            inet_conn.terminate();
        }
        this.runLock =false;
    }
}
