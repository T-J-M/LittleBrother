package moe.tjm.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;

public interface IConnProvider {
    public void connect() throws IConnException;
    public BufferedReader getReader() throws IConnException;
    public BufferedWriter getWriter() throws IConnException;
    public void terminate();
}
