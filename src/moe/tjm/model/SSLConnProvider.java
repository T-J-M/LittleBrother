package moe.tjm.model;

import moe.tjm.model.IConnException;
import moe.tjm.model.IConnProvider;

import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.Socket;

public class SSLConnProvider implements IConnProvider {

    Socket socket;
    String address;
    int port;

    public SSLConnProvider(String address, int port) throws IOException {
        socket = null;
        this.address = address;
        this.port = port;
    }

    @Override
    public String toString(){
        return "SSL "+address+":"+Integer.toString(port);
    }

    public void connect() throws IConnException{
        SSLSocketFactory ssf = (SSLSocketFactory) SSLSocketFactory.getDefault();
        try {
            socket = ssf.createSocket(socket, address, port, false);
        } catch (IOException e) {
            throw new IConnException("Error: connect "+this.toString());
        }
    }

    public BufferedReader getReader() throws IConnException{
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e){
            throw new IConnException("Error: get reader "+this.toString());
        }
        return reader;
    }

    public BufferedWriter getWriter() throws IConnException{
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e){
            throw new IConnException("Error: get reader "+this.toString());
        }
        return writer;
    }

    public void terminate(){
        try {
            socket.close();
        } catch (IOException e){
            // TODO: handle exception
        }
    }
}
